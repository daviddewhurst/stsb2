"""stsb2

A library for structural time series modeling.
"""

__version__ = '0.1.0'
__author__ = 'David Rushing Dewhurst'


from . import inference
from . import stsb
from . import util
from . import effects
