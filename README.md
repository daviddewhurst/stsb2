# Structural time series, round 2


## Modeling

`stsb2` implements a formal grammar over structural time series blocks. You can read more about the theoretical background [here](https://arxiv.org/abs/2009.06865). 

A really simple structural time series model is a random walk. Here's one:
```
t1 = 100  # how many timesteps
rw = sts.RandomWalk(t1=t1, loc=0.0, scale=1.0)
prior_samples = rw.sample(size=10)
plot_series(prior_samples);
```
![A random walk.](example/random_walk.png "A random walk.")

A more involved model involves additively decomposing a time series into three parts: a seasonal component, a trend of some kind, and noisy fluctuations. This is called a seasonal global trend (SGT) model.
```
global_trend = sts.GlobalTrend(t1=t1, a=0.0, b=0.1)
seasonal = sts.GlobalTrend(t1=t1, a=0.0, b=0.25).cos()  # create deterministic functions of blocks
noisy = sts.MA1(t1=t1, theta=0.7, scale=1.5)

sgt = global_trend + seasonal + noisy

prior_samples = sgt.sample(size=10)
plot_series(prior_samples);
```
![An SGT model.](example/sgt.png "A seasonal global trend model.")

We can also include changepoints into our models.
Here's a stochastic volatility model that incorporates two different volatilty regimes.
```
scale = 0.5
beta_1 = 0.3
beta_2 = 0.5
ic = 0.0

log_vol_1 = sts.AR1(t1=t1, beta=0.95, scale=0.5, ic=0.0)
log_vol_2 = sts.GlobalTrend(t1=t1, a=0.0, b=0.7).cos()
vol = sts.changepoint(log_vol_1.exp(), log_vol_2.exp(), frac=0.6)  # changepoint at index  int(frac * t1) = 60
price = sts.RandomWalk(t1=t1, loc=0.0, scale=vol, ic=1.0).exp()

prior_samples = price.sample(size=2)
plot_series(prior_samples);
```
![An stochastic volatility model.](example/stoch-vol.png "A stochastic volatility model.")

## Inference
`stsb2` is fundamentally "about" modeling, not inference, but we recognize that people often don't have time to implement their own inference algorithms. Inference is very much a work in progress for `stsb2`, but we have implemented basic ABC rejection sampling functionality. Look out for big updates in this area as we plan to implement SMC-ABC, gradient-free empirical Bayes, and gradient-free variational inference soon.

For now, though, let's look at us T-note yields.
```
gs10 = pdr.data.DataReader(
    'gs10',
    'fred',
    start=datetime.datetime(2000, 1, 1),
    end=datetime.datetime(2020, 10, 1)
)
train_frac = 0.7
train_ind = int(len(gs10) * train_frac)

train = gs10.values[:train_ind]
train_mean = train.mean()
train_std = train.std()
train = (train - train_mean) / train_std
test = gs10.values[train_ind:]
test = (test - train_mean) / train_std

nn = len(gs10)
train_time = range(train_ind)
test_time = range(train_ind, nn)
```
![GS10](example/gs10.png "10y")

Okay, fine. We'll pose a simple model: T-note rate can be characterized by a global trend that's additively corrupted by nonstationary noise. We have two goals:
1. Infer the trend!
2. Forecast the trend into the future. 

```
trend = sts.GlobalTrend(t1=train_ind, a=0.0, b=-1.0)
noise = sts.RandomWalk(t1=train_ind,)
gs10_model = trend + noise

# trick -- reparameterize the *data* instead of the prior
# since we're sampling, we don't have to normalize to zero mean and unit variance
prior_dist = gs10_model(size=10000)
pd_mean = prior_dist.mean()
pd_std = prior_dist.std()
stsb2.util.set_cache_mode(gs10_model, True)

renorm_train = train * pd_std + pd_mean
renorm_test = test * pd_std + pd_mean
```

We sample from the approximate posterior using rejection sampling.
```
# we would gain sample efficiency by instead using summary statistics of paths
metric = stsb2.inference.MAEDistanceMetric(
    eps_kwargs={'eps': pd_std}
)

# automate prior dist functional parameterization, but set free params manually here
guide = stsb2.inference.AutoGuide(gs10_model)
guide.distributions[trend]['a'].update_parameters(
    loc=renorm_train[0],
    scale=pd_std,
)
guide.distributions[trend]['b'].update_parameters(
    loc=(renorm_train[-1] - renorm_train[0]) / len(renorm_train),
    scale=0.5,
)
guide.distributions[noise]['scale'].update_parameters(
    loc=np.std(np.diff(renorm_train.flatten())),
    scale=0.5 * pd_std
)

# basic dumb rejection sampling -- proposal distribution is the prior
sampler = stsb2.inference.ABCSampler(
    gs10_model,
    data=renorm_train,
    guide=guide,
    verbosity=2e-4,
    metric=metric,
)

posterior, ppd = sampler.sample(nsample=35)
```
With judicious use of the `effects` library, it is easy to draw from the posterior predictive of an arbitrary component of the model and to forecast using just that component.
```
with stsb2.effects.ProposalEffect(trend):
    trend.parameter_update(
        a=posterior[trend]['a'],
        b=posterior[trend]['b']
    )
    trend_posterior = trend()
    with stsb2.effects.ForecastEffect(trend, Nt=nn - train_ind):
        trend_forecast = trend()
```
The posterior predictive and forecast distributions of the latent trend:
![gs10 latent trend](example/gs10-latent-trend.png "gs10 latent trend")

### Installation and documentation
Install: `pip install stsb2` or via [pypi](https://pypi.org/project/stsb2/).
Read [the docs](https://davidrushingdewhurst.com/stsb2/docs/) for more information.

### License
This project is released under GNU GPL 2. This means that you cannot incorporate this project into your
software without also releasing that software under GPL 2. To inquire about obtaining a license exception,
you can [email me](drd@davidrushingdewhurst.com).
